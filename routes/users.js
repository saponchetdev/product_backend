const express = require('express')
const router = express.Router()
const User = require('../models/User')

const getUsers = async function (req, res, next) {
  try {
    const users = await User.find({}).exec()
    res.status(200).json(users)
  } catch (err) {
    return res.status(500).send({
      message: err.message
    })
  }
}

const getUser = async function (req, res, next) {
  const id = req.params.id
  console.log(id)
  try {
    const user = await User.findById(id).exec()
    if (user === null) {
      return res.status(404).json({
        message: 'User not found!!!'
      })
    }
    res.json(user)
  } catch (err) {
    return res.status(404).json({
      message: err.message
    })
  }
}

const addUsers = async function (req, res, next) {
  const newUser = new User({
    username: req.body.username,
    password: req.body.password,
    roles: req.body.roles
  })
  try {
    await newUser.save()
    res.status(201).json(newUser)
  } catch (err) {
    return res.status(500).send({
      massage: err.massage
    })
  }
}
const updateUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    const user = await User.findById(userId)
    user.username = req.body.username
    user.password = req.body.password
    user.roles = req.body.roles
    await user.save()
    return res.status(200).json(user)
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }

  // const User = {
  //   id: UserID,
  //   name: req.body.name,
  //   price: parseFloat(req.body.price)
  // }
  // const index = Users.findIndex(function (item) {
  //   return item.id === UserID
  // })
  // if (index >= 0) {
  //   Users[index] = User
  //   res.json(Users[index])
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No User id ' + req.params.id
  //   })
  // }
}
const deleteUser = async function (req, res, next) {
  const userId = req.params.id
  try {
    await User.findByIdAndDelete(userId)
    res.status(204).send()
  } catch (err) {
    return res.status(404).send({ message: err.message })
  }
  // const index = Users.findIndex(function (item) {
  //   console.log(Userid)
  //   return item.id === Userid
  // })
  // if (index >= 0) {
  //   Users.splice(index, 1)
  //   res.status(204).send()
  // } else {
  //   res.status(404).json({
  //     code: 404,
  //     msg: 'No User id ' + req.params.id
  //   })
  // }
}

router.get('/', getUsers)// GET Users
router.get('/:id', getUser)// GET One User
router.post('/', addUsers)// add New Users
router.put('/:id', updateUser)// add New User
router.delete('/:id', deleteUser)

module.exports = router
